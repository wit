#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2008,2015 Sergey Poznyakoff
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import sys, os
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
import unittest
import wiki2html

class TestMarkupParserBasic (unittest.TestCase):

    def test_colon(self):
        self.assertTrue(self.__test('colon'))
        pass

    def test_headings(self):
        self.assertTrue(self.__test('headings'))
        pass

    def test_hz(self):
        self.assertTrue(self.__test('hz'))
        pass

    def test_numlist(self):
        self.assertTrue(self.__test('numlist'))
        pass

    def test_unlist(self):
        self.assertTrue(self.__test('unlist'))
        pass
    
    def test_deflist(self):
        self.assertTrue(self.__test('deflist'))
        pass
    
    def test_para(self):
        self.assertTrue(self.__test('para'))
        pass
    
    def test_it(self):
        self.assertTrue(self.__test('it'))
        pass
    
    def test_bold(self):
        self.assertTrue(self.__test('bold'))
        pass
    
    def test_boldit1(self):
        self.assertTrue(self.__test('boldit1'))
        pass

    def test_itbold1(self):
        self.assertTrue(self.__test('itbold1'))
        pass

    def test_boldit2(self):
        self.assertTrue(self.__test('boldit2'))
        pass

    def test_itbold2(self):
        self.assertTrue(self.__test('itbold2'))
        pass

    def test_boldit3(self):
        self.assertTrue(self.__test('boldit3'))
        pass

    def test_itbold3(self):
        self.assertTrue(self.__test('itbold3'))
        pass
    
    # def test_door(self):
    #     self.assertTrue(self.__test('door'))
    #     pass

    # def test_drzwi(self):
    #     self.assertTrue(self.__test('drzwi'))
    #     pass

    def __test(self, filename):
        name_in  = 'testdata/' + filename + '.wiki'
        name_out = 'testdata/' + filename + '.html'
        fh = open(name_out)
        buf = ''.join(fh.readlines()).strip()
        fh.close()
        hwm = wiki2html.HtmlWiktionaryMarkup(filename=name_in, lang="pl")
        hwm.parse()

        if str(hwm).strip() == buf:
            return True

        # fail
        print("\n>>>%s<<<" % buf)
        print(">>>%s<<<" % str(hwm).strip())
        return False

if __name__ == '__main__':
    unittest.main()

