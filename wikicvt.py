#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2008,2015 Sergey Poznyakoff
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import sys
import getopt
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from wiki2html import *
from wiki2text import *
from wiki2texi import *

class DumpWikiMarkup (WikiMarkup):
    def __str__(self):
        if self.tree:
            s = StringIO()
            self.dump(self.tree, 0, s)
            return s.getvalue()
        else:
            return ""

def usage(code=0):
    print("""usage: %s [-hvt] [-I INTYPE] [-l lang] [-o kw=val] [--lang=lang]
          [--option kw=val] [--input-type=INTYPE] [--type=OUTTYPE] [--help]
          [--verbose] file
""" % sys.argv[0])
    sys.exit(code)

handlers = {
    'dump': {
        'default': DumpWikiMarkup
    },
    'html': {
        'default': HtmlWikiMarkup,
        'wiktionary': HtmlWiktionaryMarkup
    },
    'text': {
        'default': TextWikiMarkup,
        'wiktionary': TextWiktionaryMarkup
    },
    'texi': {
        'default': TexiWikiMarkup
    }
}
    
def main():
    verbose_flag = 0
    itype = 'default'
    otype = 'html'
    lang = "pl"
    kwdict = {}
    debug = 0
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], "Dd:I:hl:o:t:v",
                                   ["dump",
                                    "debug=", "help", "lang=", "option=",
                                    "to=", "type=", "input-text", "input-type=",
                                    "verbose" ])
    except getopt.GetoptError:
        usage(1)

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
        elif o in ("-v", "--verbose"):
            verbose_flag = verbose_flag + 1
        elif o in ("-I", "--input-type"):
            itype = a
        elif o in ("-t", "--to", "--type"):
            otype = a
        elif o in ("-l", "--lang"):
            lang = a
        elif o in ("-o", "--option"):
            (kw,sep,val) = a.partition('=')
            if val != '':
                kwdict[kw] = val
        elif o == "--input-text":
            input_text = True
        elif o in ("-d", "--debug"):
            debug = eval(a)
        elif o in ("-D", "--dump"):
            otype = 'dump'

    if len(args) == 1:
        if args[0] == '-':
            kwdict['file'] = sys.stdin
        else:
            kwdict['filename'] = args[0]
    else:
        usage(1)

    kwdict['lang']=lang

    if otype in handlers:
        if itype in handlers[otype]:
            markup = handlers[otype][itype](**kwdict)
            markup.debug_level = debug
            markup.parse()
            print("%s" % str(markup))
            exit(0)
        else:
            print("unsupported input type: %s" % itype)
    else:
        print("unsupported output type: %s" % otype)
    exit(1)

if __name__ == '__main__':
    main()            
